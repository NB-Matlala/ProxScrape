import csv
import requests
from bs4 import BeautifulSoup
import random
import pyodbc
import re
import time
import datetime
from datetime import datetime
from requests.exceptions import ConnectTimeout
import math
# URL of the Autotrader website
base_url = "https://www.autotrader.co.za"

fieldnames = ['Car_ID', 'Title', 'Region', 'Make', 'Model', 'Variant', 'Suburb', 'Province', 'Price', 'ExpectedPaymentPerMonth',
              'CarType', 'RegistrationYear', 'Mileage', 'Transmission', 'FuelType', 'PriceRating', 'Dealer', 'LastUpdated',
              'PreviousOwners', 'ManufacturersColour', 'BodyType', 'ServiceHistory', 'WarrantyRemaining', 'IntroductionDate',
              'EndDate', 'ServiceIntervalDistance', 'EnginePosition', 'EngineDetail', 'EngineCapacity', 'CylinderLayoutAndQuantity',
              'FuelTypeEngine', 'FuelCapacity', 'FuelConsumption', 'FuelRange', 'PowerMaximum',
              'TorqueMaximum', 'Acceleration', 'MaximumSpeed', 'CO2Emissions', 'Version', 'DealerUrl', 'Timestamp']


# List to store the links for each car
car_links = []
car_data = []

# List to store the specifications for each car
specifications = []


proxy = "http://b7cb6a1eb0f244ba169b7a9c5020388f1df5f653:@proxy.zenrows.com:8001"
proxies = {"http": proxy, "https": proxy}


# Function to get the last page for a specific year
def get_last_page():

    #url="https://www.autotrader.co.za/cars-for-sale/kwazulu-natal/p-1"
    #response = requests.get(url, proxies=proxies, verify=False)
    
    #payload = {'api_key': '1b735384a1af55cad7af3c385026bc96', 'url': f"https://www.autotrader.co.za/cars-for-sale/kwazulu-natal/p-1"}
    #response = requests.get('http://api.scraperapi.com', params=payload)

    response = requests.get(f"https://www.autotrader.co.za/cars-for-sale/kwazulu-natal/p-{1}")
    home_page = BeautifulSoup(response.content, 'html.parser')
    parsed_html_listH = [home_page]
    total_listings_element = parsed_html_listH[0].find('span', class_='e-results-total')
    total_listings = int(total_listings_element.text.replace(' ', ''))
    listings_per_page = 20
    last_page = math.ceil(total_listings / listings_per_page)
    return last_page

base_url = "https://www.autotrader.co.za"

# Set the desired execution time to one hour (3600 seconds)
execution_time = time.time() + 150
#end_province = 9
start_province=1
province=start_province
start_page=1
page=start_page


filename = 'car_data.csv'
with open(filename, 'a', newline='', encoding='utf-8-sig') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()

    provinces_to_scrape = [1,2,3,14,5,6,7,8,9]
        # Generate the provinces_to_scrape list based on start_province
    if province == 1:
        provinces_to_scrape = [1,2,3,14,5,6,7,8,9]
    if province == 2:
        provinces_to_scrape = [2,3,14,5,6,7,8,9,1]
    if province == 3:
        provinces_to_scrape = [3, 14, 5, 6, 7, 8,9,1]
    elif province == 14:
        provinces_to_scrape = [14, 5, 6, 7, 8,9,1,2, 3]
    elif province == 5:
        provinces_to_scrape = [5, 6, 7, 8,9,1,2, 3, 14]
    elif province == 6:
        provinces_to_scrape = [6, 7, 8,9,1,2, 3, 14, 5]
    elif province == 7:
        provinces_to_scrape = [7, 8,9,1,2, 3, 14, 5, 6]
    elif province == 8:
        provinces_to_scrape = [8,9,1,2, 3, 14, 5, 6, 7]
    elif province == 9:
        provinces_to_scrape = [9,1,2, 3, 14, 5, 6, 7,8]
    for province in provinces_to_scrape:

        last_page = get_last_page()
        for page in range(start_page, 3): #last_page + 1
            #urlp="https://www.autotrader.co.za/cars-for-sale/gauteng/p-1?pagenumber={page}&sortorder=Newest&priceoption=RetailPrice"
            #response = requests.get(urlp, proxies=proxies, verify=False)
            

            #payload = {'api_key': '1b735384a1af55cad7af3c385026bc96', 'url': f"https://www.autotrader.co.za/cars-for-sale/gauteng/p-1?pagenumber={page}&sortorder=Newest&priceoption=RetailPrice"}
            #response = requests.get('http://api.scraperapi.com', params=payload)
            response = requests.get(f"https://www.autotrader.co.za/cars-for-sale/gauteng/p-{1}?pagenumber={page}&sortorder=Newest&priceoption=RetailPrice")
            home_pagep = BeautifulSoup(response.content, 'html.parser')
            parsed_html_listH = [home_pagep]

            # Find all the car listings on the page
            cars_containers = parsed_html_listH[0].find_all('div', attrs={'class': re.compile(r'b-result-tile .*')})

            for each_div in cars_containers:
                # Find the link to the car listing
                for link in each_div.find_all('a', href=True):
                    if time.time() >= execution_time :
                        execution_time += 120
                        time.sleep(60)
                    try:
                        found_link = (base_url + link['href'])
                        Car_ID = re.search(r'/(\d+)\?', found_link).group(1)
                    except:
                         continue

                    try:
                        # Get the HTML content of the car listing page
                        #res = requests.get(found_link, proxies=proxies, verify=False)
                        
                        #payload = {'api_key': '1b735384a1af55cad7af3c385026bc96', 'url': found_link}
                        #res = requests.get('http://api.scraperapi.com', params=payload)
                        
                        res = requests.get(found_link, timeout=10)
                                            # Assuming 'res.content' contains your HTML content

                        # Assuming 'res.content' contains HTML content
                        html_content = res.content.decode('utf-8')  # Decode the content to a string



                        # Define a regex pattern to match emojis
                        emoji_pattern = re.compile(
                            "["
                            "\U0001F600-\U0001F64F"  # Emoticons
                            "\U0001F300-\U0001F5FF"  # Symbols & Pictographs
                            "\U0001F680-\U0001F6FF"  # Transport & Map Symbols
                            "\U0001F700-\U0001F77F"  # Alchemical Symbols
                            "\U0001F780-\U0001F7FF"  # Geometric Shapes Extended
                            "\U0001F800-\U0001F8FF"  # Supplemental Arrows-C
                            "\U0001F900-\U0001F9FF"  # Supplemental Symbols and Pictographs
                            "\U0001FA00-\U0001FA6F"  # Chess Symbols
                            "\U0001FA70-\U0001FAFF"  # Symbols and Pictographs Extended-A
                            "\U00002702-\U000027B0"  # Dingbats
                            "\U000024C2-\U0001F251" 
                            "]+",
                            flags=re.UNICODE,
                        )

                        # Remove emojis using the regex pattern
                        cleaned_html = emoji_pattern.sub(r'', html_content)

                        # Parse the cleaned HTML content
                        car_soup = BeautifulSoup(cleaned_html, 'html.parser')

                        # Append the parsed HTML content to a list
                        parsed_html_list = [car_soup]

                        # Step : Extract data from the car listing page
                        title = parsed_html_list[0].find(class_='e-listing-title').text.strip()
                        print(Car_ID)
                        print(title)

                        ul = parsed_html_list[0].find('ul', class_='b-breadcrumbs')
                        li_items = ul.find_all('li')
                        #car_id = li_items[6].find('span', itemprop='name').text.strip()
                        location = li_items[2].text.strip()
                        brand = li_items[3].text.strip()
                        model = li_items[4].text.strip()
                        variant = li_items[5].text.strip()
                        # Find the <div class="b-rate-container"> element
                        rating_container = parsed_html_list[0].find('div', class_='b-rate-container')

                        # Find the <span> element within the suburb_div
                        ListingSuburbName = re.search(r'ga\(.+?\'dimension102\',\s*\'(.+?)\'\);', html_content)
                        suburb = ListingSuburbName.group(1)
                        price = parsed_html_list[0].find('div', class_='e-price').text.strip() if parsed_html_list[0].find('div', class_='e-price') else None
                        expected_payment = parsed_html_list[0].find('a', class_='e-calculator-link').text.strip() if parsed_html_list[0].find('a', class_='e-calculator-link') else None
                        # Check if car type is listed under a specific list item
                        li_car_type = parsed_html_list[0].find('li', class_='e-summary-icon m-type')
                        if li_car_type:
                            car_type = li_car_type.text.strip()
                        # Check if car type is listed within the specified unordered list
                        ul_car_type = parsed_html_list[0].find('ul', class_='b-icons m-large m-icon')
                        if ul_car_type:
                            li_car_type = ul_car_type.find('li', class_='e-summary-icon m-demo')
                            if li_car_type:
                                car_type = li_car_type.text.strip()
                        #car_type = parsed_html_list[0].find('li', class_='e-summary-icon m-type').text.strip() if parsed_html_list[0].find('li', class_='e-summary-icon m-type') else None
                        registration_year = parsed_html_list[0].find('li', title='Registration Year').text.strip() if parsed_html_list[0].find('li', title='Registration Year') else None
                        mileage = parsed_html_list[0].find('li', title='Mileage').text.strip().split(' ')[0] if parsed_html_list[0].find('li', title='Mileage') else None
                        transmission = parsed_html_list[0].find('li', title='Transmission').text.strip() if parsed_html_list[0].find('li', title='Transmission') else None
                        fuel_type = parsed_html_list[0].find('li', title='Fuel Type').text.strip() if parsed_html_list[0].find('li', title='Fuel Type') else None
                        price_rating = parsed_html_list[0].find('span', class_='b-price-rating').text.strip() if parsed_html_list[0].find('span', class_='b-price-rating') else None
                        dealer = parsed_html_list[0].find('a', class_='e-dealer-link').text.strip() if parsed_html_list[0].find('a', class_='e-dealer-link') else None
                        dealer_link = parsed_html_list[0].find('a', class_='e-dealer-link')
                        # Extract the link (href) attribute
                        basedealer="https://www.autotrader.co.za"
                        if dealer_link:
                            link_dealer = dealer_link
                            DealerUrl=(basedealer + link_dealer['href'])
                        else:
                            DealerUrl = None
                            car_data['DealerUrl']=DealerUrl
                        last_updated = parsed_html_list[0].find(text='Last Updated').find_next('div').text.strip() if parsed_html_list[0].find(text='Last Updated') else None
                        previous_owners = parsed_html_list[0].find(text='Previous Owners').find_next('div').text.strip() if parsed_html_list[0].find(text='Previous Owners') else None
                        manufacturers_colour = parsed_html_list[0].find(text='Colour').find_next('div').text.strip() if parsed_html_list[0].find(text='Colour') else None
                        if manufacturers_colour == None:
                            manufacturers_colour = parsed_html_list[0].find(text='Manufacturers Colour').find_next('div').text.strip() if parsed_html_list[0].find(text='Manufacturers Colour') else None
                        body_type = parsed_html_list[0].find(text='Body Type').find_next('div').text.strip() if parsed_html_list[0].find(text='Body Type') else None
                        warranty_remaining = parsed_html_list[0].find(text='Warranty Remaining').find_next('div').text.strip() if parsed_html_list[0].find(text='Warranty Remaining') else None
                        service_history = parsed_html_list[0].find(text='Service History').find_next('div').text.strip() if parsed_html_list[0].find(text='Service History') else None
                        introduction_date = parsed_html_list[0].find(text='Introduction date').find_next('span', class_='col-6').text.strip() if parsed_html_list[0].find(text='Introduction date') else None
                        end_date = parsed_html_list[0].find(text='End date').find_next('span', class_='col-6').text.strip() if parsed_html_list[0].find(text='End date') else None
                        service_interval_distance = parsed_html_list[0].find(text='Service interval distance').find_next('span', class_='col-6').text.strip() if parsed_html_list[0].find(text='Service interval distance') else None
                        engine_position = parsed_html_list[0].find(text='Engine position').find_next('span', class_='col-6').text.strip() if parsed_html_list[0].find(text='Engine position') else None
                        engine_detail = parsed_html_list[0].find(text='Engine detail').find_next('span', class_='col-6').text.strip() if parsed_html_list[0].find(text='Engine detail') else None
                        engine_capacity = parsed_html_list[0].find(text='Engine capacity (litre)').find_next('span', class_='col-6').text.strip() if parsed_html_list[0].find(text='Engine capacity (litre)') else None
                        cylinder_layout = parsed_html_list[0].find(text='Cylinder layout and quantity').find_next('span', class_='col-6').text.strip() if parsed_html_list[0].find(text='Cylinder layout and quantity') else None
                        fuel_type_engine = parsed_html_list[0].find(text='Fuel type').find_next('span', class_='col-6').text.strip() if parsed_html_list[0].find(text='Fuel type') else None
                        fuel_capacity = parsed_html_list[0].find(text='Fuel capacity').find_next('span', class_='col-6').text.strip() if parsed_html_list[0].find(text='Fuel capacity') else None
                        fuel_consumption = parsed_html_list[0].find(text='Fuel consumption (average)').find_next('span', class_='col-6').text.strip() if parsed_html_list[0].find(text='Fuel consumption (average)') else None
                        fuel_range = parsed_html_list[0].find(text='Fuel range (average)').find_next('span', class_='col-6').text.strip() if parsed_html_list[0].find(text='Fuel range (average)') else None
                        power_max = parsed_html_list[0].find(text='Power maximum (detail)').find_next('span', class_='col-6').text.strip() if parsed_html_list[0].find(text='Power maximum (detail)') else None
                        torque_max = parsed_html_list[0].find(text='Torque maximum').find_next('span', class_='col-6').text.strip() if parsed_html_list[0].find(text='Torque maximum') else None
                        acceleration = parsed_html_list[0].find(text='Acceleration 0-100 km/h').find_next('span', class_='col-6').text.strip() if parsed_html_list[0].find(text='Acceleration 0-100 km/h') else None
                        maximum_speed = parsed_html_list[0].find(text='Maximum/top speed').find_next('span', class_='col-6').text.strip() if parsed_html_list[0].find(text='Maximum/top speed') else None
                        co2_emissions = parsed_html_list[0].find(text='CO2 emissions (average)').find_next('span', class_='col-6').text.strip() if parsed_html_list[0].find(text='CO2 emissions (average)') else None
                        #current_datetime = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                        current_datetime = datetime.now().strftime('%Y-%m-%d')

                        province_names = {
                            1: 'Gauteng',
                            2: 'Kwazulu Natal',
                            3: 'Free State',
                            5: 'Mpumalanga',
                            6: 'North West',
                            7: 'Eastern Cape',
                            8: 'Northern Cape',
                            9: 'Western Cape',
                            14: 'Limpopo'
                        }
                        province_name = province_names.get(province)

                        car_data = {
                            'title': title, 'location':location,'brand': brand,'model': model, 'variant':variant, 'suburb':suburb,
                            'price':price, 'expected_payment':expected_payment,'car_type': car_type,'registration_year': registration_year,
                            'mileage':mileage,'transmission': transmission, 'fuel_type':fuel_type, 'price_rating':price_rating,
                            'dealer':dealer,'last_updated': last_updated, 'previous_owners':previous_owners,'manufacturers_colour': manufacturers_colour,
                            'body_type':body_type,'service_history':service_history, 'warranty_remaining':warranty_remaining,'introduction_date': introduction_date, 'end_date':end_date,
                            'service_interval_distance':service_interval_distance,'engine_position': engine_position,'engine_detail':engine_detail,
                            'engine_capacity':engine_capacity,'cylinder_layout': cylinder_layout, 'fuel_type_engine':fuel_type_engine, 'fuel_capacity':fuel_capacity,
                            'fuel_consumption':fuel_consumption,'fuel_range': fuel_range,'power_max': power_max, 'torque_max':torque_max,  'acceleration':acceleration,
                            'maximum_speed':maximum_speed, 'co2_emissions':co2_emissions, 'current_datetime':current_datetime,'province_name':province_name
                        }

                    except ConnectTimeout:
                        print(f"Connection timed out for link: {found_link}")
                        continue

                    car_data['Car_ID'] = Car_ID
                    car_data['DealerUrl']=DealerUrl
                    import re


                    def clean_column(column):
                        value = car_data[column]
                        if value is None:
                            return None
                        cleaned_column = car_data[column].splitlines()
                        cleaned_column = [x.split('total')[-1].strip() if 'total' in str(x) else x for x in cleaned_column]
                        cleaned_column = [x.split('max')[-1].strip() if 'max' in str(x) else x for x in cleaned_column]
                        cleaned_column = [x.split('/')[0].strip() if '/' in str(x) else x for x in cleaned_column]
                        cleaned_column = [x.split('-')[0].strip() if '-' in str(x) else x for x in cleaned_column]
                        cleaned_column = [x.split('(opt')[0].strip() if '(opt' in str(x) else x for x in cleaned_column]
                        cleaned_column = [re.sub(r'^.*?\((\d+)\).*$', r'\1', str(x)) if x is not None else x for x in cleaned_column]
                        cleaned_column = [re.sub(r'\(.*', '', str(x)) if x is not None else x for x in cleaned_column]
                        cleaned_column = [re.findall(r'^(\d+)-', str(x))[0] if re.findall(r'^\d+-', str(x)) else x for x in cleaned_column]
                        cleaned_column = [re.sub(r'[^\d.,]+', '', str(x)) if x is not None else x for x in cleaned_column]
                        cleaned_column = [x.replace('.', ',') if x is not None else x for x in cleaned_column]
                        cleaned_column = [x[0] if isinstance(x, list) else x for x in cleaned_column]
                        return cleaned_column[0] if cleaned_column else None
                    # Columns to clean
                    columns_to_clean = ['price','mileage' ,'expected_payment', 'service_interval_distance',
                                        'engine_capacity', 'fuel_capacity', 'fuel_consumption', 'fuel_range',
                                        'power_max', 'torque_max', 'acceleration','maximum_speed',
                                        'co2_emissions']

                    # Clean the specified columns in the car_data dictionary
                    for column in columns_to_clean:
                        car_data[column] = clean_column(column)

                    writer.writerow({
                        'Car_ID':car_data['Car_ID'],'Title': car_data['title'],'Region': car_data['location'],'Make': car_data['brand'],
                        'Model': car_data['model'],'Variant': car_data['variant'],'Suburb': car_data['suburb'],'Province': car_data['province_name'],'Price': car_data['price'],
                        'ExpectedPaymentPerMonth': car_data['expected_payment'], 'CarType': car_data['car_type'],'RegistrationYear': car_data['registration_year'],'Mileage': car_data['mileage'],
                        'Transmission': car_data['transmission'],'FuelType': car_data['fuel_type'],'PriceRating': car_data['price_rating'],'Dealer': car_data['dealer'],
                        'LastUpdated': car_data['last_updated'],'PreviousOwners': car_data['previous_owners'],'ManufacturersColour': car_data['manufacturers_colour'],'BodyType': car_data['body_type'],
                        'ServiceHistory':car_data['service_history'], 'WarrantyRemaining':car_data['warranty_remaining'], 'IntroductionDate':car_data['introduction_date'],
                        'EndDate':car_data['end_date'], 'ServiceIntervalDistance':car_data['service_interval_distance'], 'EnginePosition':car_data['engine_position'], 'EngineDetail':car_data['engine_detail'],
                        'EngineCapacity':car_data['engine_capacity'],'CylinderLayoutAndQuantity':car_data['cylinder_layout'], 'FuelTypeEngine':car_data['fuel_type_engine'], 'FuelCapacity':car_data['fuel_capacity'],
                        'FuelConsumption':car_data['fuel_consumption'], 'FuelRange':car_data['fuel_range'],'PowerMaximum':car_data['power_max'], 'TorqueMaximum':car_data['torque_max'],
                        'Acceleration':car_data['acceleration'], 'MaximumSpeed':car_data['maximum_speed'], 'CO2Emissions':car_data['co2_emissions'], 'Version':'1', 'DealerUrl':car_data['DealerUrl'], 'Timestamp':car_data['current_datetime']
                    })
                    #print(car_data)

        #     print(page)
        # 
        # start_page = 1
        # page=1
        # print(province)
